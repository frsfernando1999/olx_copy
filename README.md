# OLX MOBX

A project that was created to study mobx state management and Parse server 

Mobx is a state manager that focuses on using Actions to change the state of Observables, which notifies the screens and rebuilds them. 

It was also used Computed values for validation of most of the formularies, which is a value calculated based on other observable values.

It was used Parse server instead of Firebase for this application, which is a table based database.
