import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:xlo_mobx/models/ad.dart';
import 'package:xlo_mobx/stores/my_ads_store.dart';
import 'package:xlo_mobx/utils/extension.dart';

class SoldTile extends StatelessWidget {
  const SoldTile({required this.store, required this.ad, Key? key}) : super(key: key);

  final Ad? ad;
  final MyAdsStore store;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 4,
      clipBehavior: Clip.antiAlias,
      child: SizedBox(
        height: 120,
        child: Row(
          children: [
            CachedNetworkImage(
                imageUrl: ad?.images == []
                    ? 'https://triunfo.pe.gov.br/pm_tr430/wp-content/uploads/2018/03/sem-foto-300x300.jpg'
                    : ad?.images?.first,
                fit: BoxFit.contain),
            const SizedBox(
              width: 8,
            ),
            Expanded(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${ad?.title}'.capitalize(),
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(fontWeight: FontWeight.w600),
                    ),
                    Text(
                      '${ad?.price?.formattedMoney()}',
                      style: const TextStyle(fontWeight: FontWeight.w300),
                    ),
                  ],
                ),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconButton(
                  onPressed: () {
                    store.deleteAd(ad);
                  },
                  icon: const Icon(
                    Icons.delete,
                    size: 22,
                    color: Colors.purple,
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
