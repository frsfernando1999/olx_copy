import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:xlo_mobx/models/ad.dart';
import 'package:xlo_mobx/pages/ad/ad_page.dart';
import 'package:xlo_mobx/pages/create/create_page.dart';
import 'package:xlo_mobx/stores/my_ads_store.dart';

import 'package:xlo_mobx/utils/extension.dart';
import 'package:xlo_mobx/utils/nav.dart';

class ActiveTile extends StatelessWidget {
  ActiveTile({required this.ad, required this.store, Key? key})
      : super(key: key);

  final MyAdsStore store;
  final Ad? ad;

  final List<MenuChoice> choices = [
    MenuChoice(index: 0, title: 'Editar', iconData: Icons.edit),
    MenuChoice(index: 1, title: 'Já vendi', iconData: Icons.thumb_up),
    MenuChoice(index: 2, title: 'Excluir', iconData: Icons.delete),
  ];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        push(context, AdPage(ad: ad));
      },
      child: Card(
        elevation: 4,
        clipBehavior: Clip.antiAlias,
        child: SizedBox(
          height: 120,
          child: Row(
            children: [
              CachedNetworkImage(
                  imageUrl: ad?.images == []
                      ? 'https://triunfo.pe.gov.br/pm_tr430/wp-content/uploads/2018/03/sem-foto-300x300.jpg'
                      : ad?.images?.first,
                  fit: BoxFit.contain),
              const SizedBox(
                width: 8,
              ),
              Expanded(
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '${ad?.title}'.capitalize(),
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(fontWeight: FontWeight.w600),
                      ),
                      Text(
                        '${ad?.price?.formattedMoney()}',
                        style: const TextStyle(fontWeight: FontWeight.w300),
                      ),
                      Text(
                        '${ad?.views} visitas',
                        style: TextStyle(fontSize: 11, color: Colors.grey[800]),
                      ),
                    ],
                  ),
                ),
              ),
              PopupMenuButton<MenuChoice>(
                  icon: const Icon(
                    Icons.more_vert_rounded,
                    color: Colors.purple,
                  ),
                  onSelected: (choice) {
                    switch (choice.index) {
                      case 0:
                        editAd(context);
                        break;
                      case 1:
                        soldAd(context);
                        break;
                      case 2:
                        deleteAd(context);
                        break;
                    }
                  },
                  itemBuilder: (_) {
                    return choices
                        .map((choice) => PopupMenuItem<MenuChoice>(
                              value: choice,
                              child: Row(
                                children: [
                                  Icon(
                                    choice.iconData,
                                    size: 20,
                                    color: Colors.purple,
                                  ),
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  Text(
                                    choice.title,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.w400,
                                        color: Colors.purple),
                                  ),
                                ],
                              ),
                            ))
                        .toList();
                  })
            ],
          ),
        ),
      ),
    );
  }

  Future<void>? editAd(BuildContext context) async {
    final bool? success = await push(context, CreatePage(ad: ad));
    if (success != null && success) {
      store.refresh();
    }
  }

  Future<void> soldAd(BuildContext context) async {
    showDialog(
        context: context,
        builder: (_) => AlertDialog(
              title: const Text('Vendido'),
              content: Text('Confirmar a venda de ${ad?.title}?'),
              actions: [
                TextButton(
                  onPressed: Navigator.of(context).pop,
                  child: const Text('Não'),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    store.soldAd(ad);
                  },
                  child: const Text(
                    'Sim',
                    style: TextStyle(color: Colors.red),
                  ),
                ),
              ],
            ));
  }

  Future<void> deleteAd(BuildContext context) async {
    showDialog(
        context: context,
        builder: (_) => AlertDialog(
              title: const Text('Excluir?'),
              content: Text(
                  'Confirmar a exclusão de ${ad?.title}? (Esta ação não pode ser desfeita)'),
              actions: [
                TextButton(
                  onPressed: Navigator.of(context).pop,
                  child: const Text('Não'),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    store.deleteAd(ad);
                  },
                  child: const Text(
                    'Sim',
                    style: TextStyle(color: Colors.red),
                  ),
                ),
              ],
            ));
  }
}

class MenuChoice {
  MenuChoice({
    required this.index,
    required this.title,
    required this.iconData,
  });

  final int index;
  final String title;
  final IconData iconData;
}
