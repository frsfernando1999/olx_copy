import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:xlo_mobx/components/empty_card.dart';
import 'package:xlo_mobx/pages/myads/components/active_tile.dart';
import 'package:xlo_mobx/pages/myads/components/pending_tile.dart';
import 'package:xlo_mobx/pages/myads/components/sold_tile.dart';
import 'package:xlo_mobx/stores/my_ads_store.dart';

class MyAdsPage extends StatefulWidget {
  const MyAdsPage({this.initialPage = 0, Key? key}) : super(key: key);

  final int initialPage;

  @override
  State<MyAdsPage> createState() => _MyAdsPageState();
}

class _MyAdsPageState extends State<MyAdsPage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  final MyAdsStore store = MyAdsStore();

  @override
  void initState() {
    super.initState();
    _tabController =
        TabController(length: 3, vsync: this, initialIndex: widget.initialPage);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Meus Anúncios'),
          bottom: TabBar(
            controller: _tabController,
            tabs: const [
              Tab(
                child: Text('Ativos'),
              ),
              Tab(
                child: Text('Pendentes'),
              ),
              Tab(
                child: Text('Vendidos'),
              ),
            ],
          ),
        ),
        body: Observer(builder: (_) {
          if (store.loading) {
            return const Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.white),
              ),
            );
          } else {
            return TabBarView(
              controller: _tabController,
              children: [
                Observer(builder: (_) {
                  if ((store.activeAds as List).isEmpty) {
                    return const EmptyCard(text: 'ativo');
                  } else {
                    return ListView.builder(
                      itemCount: store.activeAds?.length,
                      itemBuilder: (_, index) {
                        return ActiveTile(
                            ad: store.activeAds?[index], store: store);
                      },
                    );
                  }
                }),
                Observer(builder: (_) {
                  if ((store.pendingAds as List).isEmpty) {
                    return const EmptyCard(text: 'pendente');
                  } else {
                    return ListView.builder(
                      itemCount: store.pendingAds?.length,
                      itemBuilder: (_, index) {
                        return PendingTile(ad: store.pendingAds?[index]);
                      },
                    );
                  }
                }),
                Observer(builder: (_) {
                  if ((store.soldAds as List).isEmpty) {
                    return const EmptyCard(text: 'vendido');
                  } else {
                    return ListView.builder(
                      itemCount: store.soldAds?.length,
                      itemBuilder: (_, index) {
                        return SoldTile(
                          ad: store.soldAds?[index],
                          store: store,
                        );
                      },
                    );
                  }
                }),
              ],
            );
          }
        }));
  }
}
