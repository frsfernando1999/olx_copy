import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:xlo_mobx/pages/filter/components/section_title.dart';
import 'package:xlo_mobx/stores/filter_store.dart';

class VendorTypeField extends StatelessWidget {
  const VendorTypeField({required this.filter, Key? key}) : super(key: key);

  final FilterStore filter;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        const SectionTitle(title: 'Tipo de anunciante'),
        Observer(
          builder: (BuildContext context) {
            return Wrap(
              runSpacing: 4,
              children: [
                GestureDetector(
                  onTap: selectParticular,
                  child: Container(
                    height: 50,
                    width: 130,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        color: filter.isTypeParticular
                            ? Colors.purple
                            : Colors.transparent,
                        border: Border.all(
                          color: filter.isTypeParticular
                              ? Colors.purple
                              : Colors.grey,
                        )),
                    child: Text(
                      'Particular',
                      style: TextStyle(
                          color: filter.isTypeParticular
                              ? Colors.white
                              : Colors.black),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 12,
                ),
                GestureDetector(
                  onTap: selectProfessional,
                  child: Container(
                    height: 50,
                    width: 130,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        color: filter.isTypeProfessional
                            ? Colors.purple
                            : Colors.transparent,
                        border: Border.all(
                          color: filter.isTypeProfessional
                              ? Colors.purple
                              : Colors.grey,
                        )),
                    child: Text(
                      'Profissional',
                      style: TextStyle(
                          color: filter.isTypeProfessional
                              ? Colors.white
                              : Colors.black),
                    ),
                  ),
                ),
              ],
            );
          },
        )
      ],
    );
  }

  void selectParticular() {
    if (filter.isTypeParticular) {
      if(filter.isTypeProfessional){
        filter.resetVendorType(vendorTypeParticular);
      }else{
        filter.selectVendorType(vendorTypeProfessional);
      }
    } else {
      filter.setVendorType(vendorTypeParticular);
    }
  }

  void selectProfessional() {
    if (filter.isTypeProfessional) {
      if(filter.isTypeParticular){
        filter.resetVendorType(vendorTypeProfessional);
      }else{
        filter.selectVendorType(vendorTypeParticular);
      }
    } else {
      filter.setVendorType(vendorTypeProfessional);
    }
  }
}
