import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:xlo_mobx/pages/filter/components/price_text_field.dart';
import 'package:xlo_mobx/pages/filter/components/section_title.dart';
import 'package:xlo_mobx/stores/filter_store.dart';

class PriceRangeField extends StatelessWidget {
  const PriceRangeField({required this.filter, Key? key}) : super(key: key);

  final FilterStore filter;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SectionTitle(title: 'Preço'),
        Row(
          children: [
            PriceTextField(
              hint: 'Min',
              onChanged: filter.setMinPrice,
              initialValue: filter.minPrice,
            ),
            const SizedBox(
              width: 12,
            ),
            PriceTextField(
              hint: 'Max',
              onChanged: filter.setMaxPrice,
              initialValue: filter.maxPrice,
            ),
          ],
        ),
        Observer(builder: (_) {
          if (filter.priceError != null) {
            return Padding(
              padding: const EdgeInsets.only(top: 4),
              child: Text(
                "${filter.priceError}",
              style: const TextStyle(color: Colors.red,
              fontSize: 12,),),
            );
          } else {
            return Container();
          }
        }),
      ],
    );
  }
}
