import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PriceTextField extends StatelessWidget {
  const PriceTextField({this.initialValue, required this.hint, required this.onChanged, Key? key}) : super(key: key);

  final String hint;
  final int? initialValue;
  final Function(int?) onChanged;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: TextFormField(
        initialValue: initialValue?.toString(),
        style: const TextStyle(fontSize: 16),
        keyboardType: TextInputType.number,
        maxLength: 7,
        inputFormatters: [
          FilteringTextInputFormatter.digitsOnly,
        ],
        decoration: InputDecoration(
          isDense: true,
          counterText: '',
          prefixText: 'R\$ ',
          labelText: hint,
          border: const OutlineInputBorder(),
        ),
        onChanged: (text) {
          onChanged(int.tryParse(text.replaceAll('.', '')));
        },
      ),
    );
  }
}
