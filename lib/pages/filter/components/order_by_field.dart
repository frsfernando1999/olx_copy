import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:xlo_mobx/pages/filter/components/section_title.dart';
import 'package:xlo_mobx/stores/filter_store.dart';

class OrderByField extends StatelessWidget {
  const OrderByField({required this.filter, Key? key}) : super(key: key);

  final FilterStore filter;

  Widget buildOption(String title, OrderBy option) {
    return GestureDetector(
      onTap: () {
        filter.setOrderBy(option);
      },
      child: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(horizontal: 24),
        height: 50,
        decoration: BoxDecoration(
          border:
              filter.orderBy == option ? null : Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(25),
          color: filter.orderBy == option ? Colors.purple : Colors.transparent,
        ),
        child: Text(
          title,
          style: TextStyle(
            color: filter.orderBy == option ? Colors.white : Colors.black,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SectionTitle(
          title: 'Ordenar por: ',
        ),
        Observer(builder: (_) {
          return Row(
            children: [
              buildOption('Data', OrderBy.date),
              const SizedBox(width: 12),
              buildOption('Preço', OrderBy.price),
            ],
          );
        }),
      ],
    );
  }
}
