import 'package:flutter/material.dart';
import 'package:xlo_mobx/models/ad.dart';

import 'package:xlo_mobx/utils/extension.dart';

class UserPanel extends StatelessWidget {
  const UserPanel({required this.ad, Key? key}) : super(key: key);

  final Ad? ad;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 16,right: 16,left: 16, bottom: ad?.status == AdStatus.pending ? 20 : 100),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const Padding(
            padding: EdgeInsets.only(bottom: 18),
            child: Text(
              'Anunciante',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(16),
            color: Colors.grey[200],
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "${ad?.user?.name}".capitalize(),
                  style: const TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.w600),
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  'Na XLO desde ${ad?.user?.createdAt.formattedDate()}',
                  style: const TextStyle(
                    color: Colors.grey,
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
