import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:xlo_mobx/models/ad.dart';

class LocationPanel extends StatelessWidget {
  const LocationPanel({required this.ad, Key? key}) : super(key: key);
  final Ad? ad;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 16, right: 16, left: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(vertical: 18),
            child: Text(
              'Localização',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          Row(
            children: [
              Expanded(
                child: Column(
                  children: const [
                    Text('CEP'),
                    SizedBox(
                      height: 12,
                    ),
                    Text('Município'),
                    SizedBox(
                      height: 12,
                    ),
                    Text('Bairro'),
                  ],
                  crossAxisAlignment: CrossAxisAlignment.start,
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: Column(
                    children: [
                      Text('${ad?.address?.cep}'),
                      const SizedBox(
                        height: 12,
                      ),
                      Text('${ad?.address?.city?.name}'),
                      const SizedBox(
                        height: 12,
                      ),
                      Text('${ad?.address?.district}'),
                    ],
                    crossAxisAlignment: CrossAxisAlignment.start,
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
