import 'package:flutter/material.dart';
import 'package:xlo_mobx/models/ad.dart';
import 'package:xlo_mobx/pages/create/components/description_text.dart';

class DescriptionPanel extends StatelessWidget {
  const DescriptionPanel({required this.ad, Key? key}) : super(key: key);

  final Ad? ad;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 18, left: 18, right: 18),
          child: Text(
            'Descrição',
            textAlign: TextAlign.start,
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0, bottom: 18),
            child: DescriptionTextWidget(text: ad?.description),
          ),
      ],
    );
  }
}
