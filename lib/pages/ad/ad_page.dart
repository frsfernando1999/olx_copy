import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';
import 'package:xlo_mobx/models/ad.dart';
import 'package:xlo_mobx/pages/ad/components/bottom_bar.dart';
import 'package:xlo_mobx/pages/ad/components/location_panel.dart';
import 'package:xlo_mobx/pages/ad/components/main_panel.dart';
import 'package:xlo_mobx/pages/ad/components/description_panel.dart';
import 'package:xlo_mobx/pages/ad/components/user_panel.dart';
import 'package:xlo_mobx/stores/favorite_store.dart';
import 'package:xlo_mobx/stores/user_manager_store.dart';

class AdPage extends StatefulWidget {
  const AdPage({required this.ad, Key? key}) : super(key: key);

  final Ad? ad;

  @override
  State<AdPage> createState() => _AdPageState();
}

class _AdPageState extends State<AdPage> {
  late PageController _pageController;
  final UserManagerStore userManagerStore = GetIt.I<UserManagerStore>();
  final FavoriteStore favoriteStore = GetIt.I<FavoriteStore>();
  int activePage = 0;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(viewportFraction: 0.9, initialPage: 0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 6,
        title: const Text('Anúncio'),
        actions: [
          if (widget.ad?.status == AdStatus.active &&
              userManagerStore.isLoggedIn)
            Observer(builder: (_) {
              return IconButton(
                onPressed: () => favoriteStore.toggleFavorite(widget.ad),
                icon: Icon(
                  favoriteStore.favoriteList
                          .any((favAd) => favAd?.id == widget.ad?.id)
                      ? Icons.favorite
                      : Icons.favorite_border,
                ),
              );
            })
        ],
      ),
      body: Stack(
        children: [
          ListView(
            children: [
              adImages(),
              adMainPanel(),
              Divider(
                color: Colors.grey[500],
              ),
              adDescriptionPanel(),
              Divider(
                color: Colors.grey[500],
              ),
              adLocationPanel(),
              Divider(
                color: Colors.grey[500],
              ),
              adAdOwnerPanel(),
            ],
          ),
          BottomBar(ad: widget.ad),
        ],
      ),
    );
  }

  adAdOwnerPanel() {
    return UserPanel(ad: widget.ad);
  }

  adLocationPanel() {
    return LocationPanel(ad: widget.ad);
  }

  adDescriptionPanel() {
    return DescriptionPanel(ad: widget.ad);
  }

  Padding adMainPanel() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: MainPanel(ad: widget.ad),
    );
  }

  Column adImages() {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 1),
          height: 280,
          child: PageView.builder(
            pageSnapping: true,
            controller: _pageController,
            onPageChanged: (page) {
              setState(() {
                activePage = page;
              });
            },
            itemCount: widget.ad?.images?.length,
            itemBuilder: (_, index) {
              return Container(
                margin: const EdgeInsets.symmetric(horizontal: 2),
                child: CachedNetworkImage(
                  imageUrl: widget.ad?.images?[index],
                  fit: BoxFit.cover,
                ),
              );
            },
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: indicators(widget.ad?.images?.length, activePage).toList(),
        )
      ],
    );
  }

  List<Widget> indicators(imagesLength, currentIndex) {
    return List<Widget>.generate(imagesLength, (index) {
      return Container(
        margin: const EdgeInsets.symmetric(horizontal: 3, vertical: 8),
        width: 10,
        height: 10,
        decoration: BoxDecoration(
          color: currentIndex == index ? Colors.black : Colors.black26,
          shape: BoxShape.circle,
        ),
      );
    });
  }
}
