import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';
import 'package:xlo_mobx/components/error_box.dart';
import 'package:xlo_mobx/models/category.dart';
import 'package:xlo_mobx/stores/category_store.dart';

class CategoryScreen extends StatelessWidget {
  CategoryScreen({this.showAll = true, this.selected, Key? key})
      : super(key: key);

  final Category? selected;
  final bool showAll;
  final CategoryStore categoryStore = GetIt.I<CategoryStore>();

  @override
  Widget build(BuildContext context) {
    final node = FocusScope.of(context);
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: SizedBox(
        height: MediaQuery.of(context).size.height * 0.5,
        width: MediaQuery.of(context).size.width * 0.4,
        child: Center(
          child: Observer(
            builder: (_) {
              if (categoryStore.error != null) {
                return ErrorBox(
                  message: categoryStore.error,
                );
              } else if (categoryStore.categoryList.isEmpty) {
                return const Center(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(Colors.white),
                  ),
                );
              } else {
                final categories = showAll
                    ? categoryStore.allCategoryList
                    : categoryStore.categoryList;
                return ListView.separated(
                  shrinkWrap: true,
                    separatorBuilder: (_, __) {
                      return const Divider(
                        height: 0.2,
                        color: Colors.grey,
                      );
                    },
                    itemCount: categories.length,
                    itemBuilder: (_, index) {
                      final category = categories[index];
                      return InkWell(
                        onTap: () {
                          Navigator.of(context).pop(category);
                          node.unfocus();
                        },
                        child: Container(
                          height: 50,
                          color: category.id == selected?.id
                              ? Colors.purple.withAlpha(50)
                              : null,
                          alignment: Alignment.center,
                          child: Text(
                            "${category.description}",
                            style: TextStyle(
                                color: Colors.grey[700],
                                fontWeight: (category.id == selected?.id)
                                    ? FontWeight.bold
                                    : null),
                          ),
                        ),
                      );
                    });
              }
            },
          ),
        ),
      ),
    );
  }
}
