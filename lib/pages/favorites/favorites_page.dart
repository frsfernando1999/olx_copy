import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';
import 'package:xlo_mobx/components/custom_drawer/custom_drawer.dart';
import 'package:xlo_mobx/components/empty_card.dart';
import 'package:xlo_mobx/pages/favorites/components/favorite_tile.dart';
import 'package:xlo_mobx/stores/favorite_store.dart';

class FavoritePage extends StatelessWidget {
  FavoritePage({this.hideDrawer = false, Key? key}) : super(key: key);

  final FavoriteStore favoriteStore = GetIt.I<FavoriteStore>();

  final bool hideDrawer;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Favoritos'),
      ),
      drawer: hideDrawer ? null : const CustomDrawer(),
      body: Observer(builder: (_) {
        if (favoriteStore.favoriteList.isEmpty) {
          return EmptyCard(text: 'favoritado');
        } else {
          return ListView.builder(
            padding: const EdgeInsets.all(2),
            itemCount: favoriteStore.favoriteList.length,
            itemBuilder: (_, index) =>
                FavoriteTile(ad: favoriteStore.favoriteList[index]),
          );
        }
      }),
    );
  }
}
