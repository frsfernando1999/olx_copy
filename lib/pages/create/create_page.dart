import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';
import 'package:mobx/mobx.dart';
import 'package:xlo_mobx/components/custom_drawer/custom_drawer.dart';
import 'package:xlo_mobx/components/error_box.dart';
import 'package:xlo_mobx/models/ad.dart';
import 'package:xlo_mobx/pages/create/components/category_field.dart';
import 'package:xlo_mobx/pages/create/components/cep_field.dart';
import 'package:xlo_mobx/pages/create/components/hide_phone_field.dart';
import 'package:xlo_mobx/pages/create/components/images_field.dart';
import 'package:xlo_mobx/pages/myads/my_ads_page.dart';
import 'package:xlo_mobx/stores/create_store.dart';
import 'package:xlo_mobx/stores/page_store.dart';
import 'package:xlo_mobx/utils/nav.dart';

class CreatePage extends StatefulWidget {
  const CreatePage({this.ad, Key? key}) : super(key: key);

  final Ad? ad;

  @override
  State<CreatePage> createState() => _CreatePageState(ad: ad);
}

class _CreatePageState extends State<CreatePage> {
  _CreatePageState({Ad? ad})
      : editing = ad != null,
        createStore = CreateStore(ad);

  final CreateStore createStore;
  bool editing;

  @override
  void initState() {
    super.initState();
    //when triggers only once
    when((_) => createStore.savedAd, () {
      if (editing) {
        Navigator.of(context).pop(true);
      } else {
        GetIt.I<PageStore>().setPage(0);
        push(context, const MyAdsPage(initialPage: 1));
      }
    });
  }

  final labelStyle = const TextStyle(
    fontWeight: FontWeight.w800,
    color: Colors.grey,
    fontSize: 16,
  );

  final contentPadding = const EdgeInsets.fromLTRB(16, 10, 12, 10);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(editing ? 'Editando Anúncio' : 'Criar Anúncio'),
        centerTitle: true,
      ),
      drawer: editing ? null : const CustomDrawer(),
      body: Center(
        child: SingleChildScrollView(
          child: Card(
            clipBehavior: Clip.antiAlias,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
            elevation: 10,
            margin: const EdgeInsets.symmetric(horizontal: 16),
            child: Observer(builder: (_) {
              if (createStore.loading) {
                return Padding(
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    children: const [
                      Text(
                        'Salvando anúncio',
                        style: TextStyle(fontSize: 18, color: Colors.purple),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation(Colors.purple),
                      )
                    ],
                  ),
                );
              } else {
                return Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    ImagesField(createStore),
                    Padding(
                      padding: contentPadding,
                      child: const Text(
                        'Selecione entre 1 a 5 imagens.',
                        style: TextStyle(fontSize: 15, color: Colors.grey),
                      ),
                    ),
                    Observer(builder: (_) {
                      return TextFormField(
                        onChanged: createStore.setTitle,
                        autofocus: false,
                        initialValue: createStore.title,
                        decoration: InputDecoration(
                          errorText: createStore.titleError,
                          labelText: 'Título *',
                          labelStyle: labelStyle,
                          contentPadding: contentPadding,
                        ),
                      );
                    }),
                    Observer(builder: (_) {
                      return TextFormField(
                        initialValue: createStore.description,
                        onChanged: createStore.setDescription,
                        maxLength: 250,
                        decoration: InputDecoration(
                          errorText: createStore.descriptionError,
                          labelText: 'Descrição (Detalhes do produto)*',
                          labelStyle: labelStyle,
                          contentPadding: contentPadding,
                        ),
                        minLines: 1,
                        maxLines: 7,
                      );
                    }),
                    CategoryField(createStore: createStore),
                    CepField(createStore: createStore, editing: editing),
                    Observer(builder: (_) {
                      return TextFormField(
                        initialValue: createStore.priceText,
                        onChanged: createStore.setPrice,
                        decoration: InputDecoration(
                          errorText: createStore.priceError,
                          labelText: 'Preço *',
                          labelStyle: labelStyle,
                          contentPadding: contentPadding,
                          prefixText: 'R\$ ',
                        ),
                        maxLength: 10,
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(
                              RegExp(r'^\d+[\,\.]?\d{0,2}')),
                        ],
                        keyboardType: const TextInputType.numberWithOptions(
                            decimal: true),
                      );
                    }),
                    HidePhoneField(createStore: createStore),
                    Observer(builder: (_) {
                      return ErrorBox(
                        message: createStore.error,
                      );
                    }),
                    Observer(builder: (_) {
                      return SizedBox(
                        height: 50,
                        child: GestureDetector(
                          onTap: createStore.invalidSendPressed,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                            ),
                            onPressed: createStore.sendPressed,
                            child: const Text(
                              'Enviar',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16),
                            ),
                          ),
                        ),
                      );
                    })
                  ],
                );
              }
            }),
          ),
        ),
      ),
    );
  }
}
