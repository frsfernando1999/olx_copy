import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:xlo_mobx/pages/category/category_page.dart';
import 'package:xlo_mobx/stores/create_store.dart';

class CategoryField extends StatelessWidget {
  const CategoryField({this.createStore, Key? key}) : super(key: key);

  final CreateStore? createStore;

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) {
        return Column(
          children: [
            ListTile(
              title: Text(
                'Categoria *',
                style: TextStyle(
                  fontWeight: createStore?.category == null
                      ? FontWeight.w800
                      : FontWeight.w700,
                  color: Colors.grey,
                  fontSize: createStore?.category == null ? 16 : 13,
                ),
              ),
              subtitle: createStore?.category == null
                  ? null
                  : Text(
                      '${createStore?.category?.description}',
                      style: const TextStyle(fontSize: 16, color: Colors.black),
                    ),
              trailing: const Icon(Icons.keyboard_arrow_down),
              onTap: () async {
                final category = await showDialog(
                    context: context,
                    builder: (_) => CategoryScreen(
                          showAll: false,
                          selected: createStore?.category,
                        ));
                if (category != null) {
                  createStore?.setCategory(category);
                }
              },
            ),
            if (createStore?.categoryError != null)
              Container(
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.fromLTRB(16, 8, 0, 4),
                decoration: const BoxDecoration(
                    border: Border(
                  top: BorderSide(color: Colors.red),
                )),
                child: Text(
                  createStore?.categoryError as String,
                  style: const TextStyle(
                    color: Colors.red,
                    fontSize: 12,
                  ),
                ),
              )
            else
              Container(
              decoration: BoxDecoration(
                border: Border(top: BorderSide(color: Colors.grey[500]!)),
              ),
            )
          ],
        );
      },
    );
  }
}
