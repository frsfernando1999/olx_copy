import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:xlo_mobx/stores/create_store.dart';

class HidePhoneField extends StatelessWidget {
  const HidePhoneField({required this.createStore, Key? key}) : super(key: key);

  final CreateStore createStore;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Row(
        children: [
          Observer(
            builder: (BuildContext context) {
              return Checkbox(
                value: createStore.hidePhone,
                onChanged: createStore.setHidePhone,
              );
            },
          ),
          const Expanded(child: Text('Ocultar o meu telefone neste anúncio')),
        ],
      ),
    );
  }
}
