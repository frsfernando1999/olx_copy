import 'package:flutter/material.dart';

class DescriptionTextWidget extends StatefulWidget {
  const DescriptionTextWidget({required this.text, Key? key}) : super(key: key);

  final String? text;

  @override
  State<DescriptionTextWidget> createState() => _DescriptionTextWidgetState();
}

class _DescriptionTextWidgetState extends State<DescriptionTextWidget> {
  String? firstPart;
  String? secondPart;
  bool showingMore = true;
  final TextStyle descriptionStyle = const TextStyle(
    fontSize: 18,
  );

  @override
  void initState() {
    super.initState();
    if (widget.text != null) {
      if ((widget.text as String).length > 100) {
        firstPart = widget.text?.substring(0, 100);
        secondPart = widget.text?.substring(100, widget.text?.length);
      } else {
        firstPart = widget.text;
        secondPart = "";
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: "$secondPart".isEmpty
          ? Text("$firstPart",textAlign: TextAlign.justify, style: descriptionStyle)
          : Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  showingMore ? ("$firstPart...") : ("$firstPart$secondPart"),
                  style: descriptionStyle, textAlign: TextAlign.justify,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: InkWell(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          showingMore ? "Mostrar mais" : "Mostrar Menos",
                          style:
                              const TextStyle(color: Colors.purple, fontSize: 16),
                        ),
                      ],
                    ),
                    onTap: () {
                      setState(() {
                        showingMore = !showingMore;
                      });
                    },
                  ),
                ),
              ],
            ),
    );
  }
}
