import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:xlo_mobx/pages/create/components/image_dialog.dart';
import 'package:xlo_mobx/pages/create/components/image_source_model.dart';
import 'package:xlo_mobx/stores/create_store.dart';

class ImagesField extends StatelessWidget {
  const ImagesField(this.createStore, {Key? key}) : super(key: key);

  final CreateStore createStore;

  @override
  Widget build(BuildContext context) {
    void onImageSelected(File? image) {
      createStore.images.add(image);
      Navigator.of(context).pop();
    }

    return Column(
      children: [
        Container(
          color: Colors.grey[200],
          height: 120,
          child: Observer(
            builder: (_) {
              return ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: createStore.images.length < 5
                    ? createStore.images.length + 1
                    : 5,
                itemBuilder: (_, int index) {
                  if (index == createStore.images.length) {
                    return unselectedImageField(
                        context, onImageSelected, index);
                  } else {
                    return selectedImageField(index, context);
                  }
                },
              );
            },
          ),
        ),
        Observer(builder: (_) {
          if (createStore.imagesError != null) {
            return Container(
              alignment: Alignment.centerLeft,
              padding: const EdgeInsets.fromLTRB(16, 8, 0, 4),
              decoration: const BoxDecoration(
                  border: Border(
                top: BorderSide(color: Colors.red),
              )),
              child: Text(
                "${createStore.imagesError}",
                style: const TextStyle(
                  color: Colors.red,
                  fontSize: 12,
                ),
              ),
            );
          }
          return Container();
        }),
      ],
    );
  }

  Padding unselectedImageField(BuildContext context,
      void Function(File? image) onImageSelected, int index) {
    return Padding(
      padding: EdgeInsets.fromLTRB(8, 8, (index == 4 || index == 3) ? 8 : 0, 8),
      child: GestureDetector(
        onTap: () {
          if (Platform.isAndroid) {
            showModalBottomSheet(
              context: context,
              builder: (_) =>
                  ImageSourceModel(onImageSelected: onImageSelected),
            );
          } else {
            showCupertinoModalPopup(
              context: context,
              builder: (_) =>
                  ImageSourceModel(onImageSelected: onImageSelected),
            );
          }
        },
        child: Align(
          alignment: Alignment.center,
          child: Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(12)),
              color: Colors.grey[400],
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Icon(
                  Icons.camera_alt,
                  size: 50,
                  color: Colors.white,
                ),
                Text(
                  'Imagem',
                  style: TextStyle(color: Colors.white),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Padding selectedImageField(int index, BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(8, 8, index == 4 ? 8 : 0, 8),
      child: GestureDetector(
        onTap: () {
          showDialog(
              context: context,
              builder: (_) => ImageDialog(
                    image: createStore.images[index],
                    onDelete: () => createStore.images.removeAt(index),
                  ));
        },
        child: Align(
          alignment: Alignment.center,
          child: Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(12)),
              color: Colors.grey[400],
            ),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(12)),
                image: createStore.images[index] is File
                    ? DecorationImage(
                        image: FileImage(createStore.images[index]))
                    : DecorationImage(
                        image: NetworkImage(createStore.images[index])),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
