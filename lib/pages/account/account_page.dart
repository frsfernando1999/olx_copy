import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';
import 'package:xlo_mobx/components/custom_drawer/custom_drawer.dart';
import 'package:xlo_mobx/pages/edit_account/edit_account_page.dart';
import 'package:xlo_mobx/pages/favorites/favorites_page.dart';
import 'package:xlo_mobx/pages/myads/my_ads_page.dart';
import 'package:xlo_mobx/stores/account_store.dart';
import 'package:xlo_mobx/stores/page_store.dart';
import 'package:xlo_mobx/stores/user_manager_store.dart';
import 'package:xlo_mobx/utils/extension.dart';
import 'package:xlo_mobx/utils/get_phone_region.dart';
import 'package:xlo_mobx/utils/nav.dart';

class AccountPage extends StatelessWidget {
  const AccountPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final AccountStore store = AccountStore();
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Minha Conta',
          style: TextStyle(fontWeight: FontWeight.w600),
        ),
        actions: [
          Container(
            padding: const EdgeInsets.all(16),
            alignment: Alignment.center,
            child: InkWell(
                onTap: () {
                  push(context, const EditAccountScreen());
                },
                child: const Text(
                  'EDITAR',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                      fontWeight: FontWeight.w600),
                )),
          )
        ],
      ),
      backgroundColor: Colors.white,
      drawer: const CustomDrawer(),
      body: _body(context, store),
    );
  }

  _body(context, AccountStore store) {
    final UserManagerStore userManagerStore = GetIt.I<UserManagerStore>();
    if (userManagerStore.isLoggedIn) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          accountTopPanel(context, userManagerStore),
          ListTile(
            onTap: () {
              push(context, const MyAdsPage());
            },
            leading: const Text(
              'Meus Anúncios',
              style: TextStyle(
                color: Colors.purple,
                fontSize: 16,
                fontWeight: FontWeight.w600,
              ),
            ),
            trailing: const Icon(Icons.keyboard_arrow_right),
          ),
          ListTile(
            onTap: () {
              push(context, FavoritePage(hideDrawer: true));
            },
            leading: const Text(
              'Favoritos',
              style: TextStyle(
                color: Colors.purple,
                fontSize: 16,
                fontWeight: FontWeight.w600,
              ),
            ),
            trailing: const Icon(Icons.keyboard_arrow_right),
          ),
          ListTile(
            onTap: () {
              store.logout();
              GetIt.I<PageStore>().setPage(0);
            },
            leading: const Text(
              'Log Out',
              style: TextStyle(
                color: Colors.purple,
                fontSize: 16,
                fontWeight: FontWeight.w600,
              ),
            ),
            trailing: const Icon(Icons.keyboard_arrow_right),
          ),
        ],
      );
    } else {
      return Container();
    }
  }

  Container accountTopPanel(context, UserManagerStore userManagerStore) {
    return Container(
      alignment: Alignment.center,
      height: MediaQuery.of(context).size.height * 0.3,
      color: Colors.purple,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Observer(builder: (_) {
            return Text(
              '${userManagerStore.user?.name}'.capitalize(),
              style: const TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                  fontWeight: FontWeight.w600),
            );
          }),
          const SizedBox(
            height: 8,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Icon(
                Icons.location_on_sharp,
                color: Colors.white,
              ),
              const SizedBox(
                width: 8,
              ),
              Observer(builder: (_) {
                return Text(
                  getPhoneRegion(userManagerStore.user?.phone),
                  style: const TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                      fontWeight: FontWeight.w500),
                );
              }),
            ],
          ),
        ],
      ),
    );
  }
}
