import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mobx/mobx.dart';
import 'package:xlo_mobx/pages/edit_account/components/user_type_switch.dart';
import 'package:xlo_mobx/stores/edit_account_store.dart';
import 'package:xlo_mobx/utils/mask.dart';

class EditAccountScreen extends StatefulWidget {
  const EditAccountScreen({Key? key}) : super(key: key);

  @override
  State<EditAccountScreen> createState() => _EditAccountScreenState();
}

class _EditAccountScreenState extends State<EditAccountScreen> {
  final EditAccountStore store = EditAccountStore();

  @override
  void initState() {
    super.initState();
    when((_) => (store.editSuccessful), (){
      Navigator.of(context).pop();
      Fluttertoast.showToast(
          msg: "Perfil modificado com sucesso",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 5,
          backgroundColor: Colors.black45,
          textColor: Colors.white,
          fontSize: 16.0
      );

    });
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text('Editar a conta'),
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const Text(
                'Dados da conta',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              const Text(
                'Nome Completo',
                style: TextStyle(fontSize: 16),
              ),
              const SizedBox(
                height: 8,
              ),
              Observer(builder: (_) {
                return TextFormField(
                  initialValue: store.name,
                  enabled: !store.loading,
                  onChanged: store.setName,
                  decoration: InputDecoration(
                    errorText: store.nameError,
                    border: const OutlineInputBorder(),
                    isDense: true,
                  ),
                );
              }),
              const SizedBox(
                height: 12,
              ),
              UserTypeSwitch(store: store),
              const SizedBox(
                height: 12,
              ),
              const Text(
                'Celular: ',
                style: TextStyle(fontSize: 16),
              ),
              const SizedBox(
                height: 8,
              ),
              Observer(builder: (_) {
                return TextFormField(
                  enabled: !store.loading,
                  maxLength: 15,
                  initialValue: store.phone,
                  onChanged: store.setPhone,
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    FilteringTextInputFormatter.digitsOnly,
                    MaskTextInputFormatter(mask: '(##) #####-####')
                  ],
                  decoration: InputDecoration(
                    enabled: !store.loading,
                    hintText: 'Novo numero',
                    counterText: '',
                    errorText: store.phoneError,
                    border: const OutlineInputBorder(),
                    isDense: true,
                  ),
                );
              }),
              const SizedBox(
                height: 12,
              ),
              Observer(builder: (_) {
                return TextFormField(
                  enabled: !store.loading,
                  obscureText: true,
                  onChanged: store.setPass1,
                  decoration: InputDecoration(
                    errorText: store.passError,
                    border: const OutlineInputBorder(),
                    hintText: 'Nova senha',
                    isDense: true,
                  ),
                );
              }),
              const SizedBox(
                height: 12,
              ),
              Observer(builder: (_) {
                return TextFormField(
                  enabled: !store.loading,
                  obscureText: true,
                  onChanged: store.setPass2,
                  decoration: InputDecoration(
                    errorText: store.passError,
                    border: const OutlineInputBorder(),
                    hintText: 'Repita a nova senha',
                    isDense: true,
                  ),
                );
              }),
              const SizedBox(
                height: 12,
              ),
              Observer(
                builder: (_) {
                  return SizedBox(
                    height: 45,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30))),
                      onPressed: store.savePressed,
                      child: store.loading
                          ? const Text('Salvando...',
                              style: TextStyle(fontSize: 18))
                          : const Text('Salvar',
                              style: TextStyle(fontSize: 18)),
                    ),
                  );
                },
              ),
              const SizedBox(
                height: 12,
              ),
              Observer(builder: (_) {
                return SizedBox(
                  height: 45,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: Colors.red,
                        onSurface: Colors.red.withAlpha(120),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30))),
                    onPressed: store.loading
                        ? null
                        : () {
                            Navigator.of(context).pop();
                          },
                    child: const Text(
                      'Sair',
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                );
              }),
            ],
          ),
        ),
      ),
    );
  }
}
