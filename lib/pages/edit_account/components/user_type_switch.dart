import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';
import 'package:xlo_mobx/models/user.dart';
import 'package:xlo_mobx/stores/edit_account_store.dart';
import 'package:xlo_mobx/stores/user_manager_store.dart';

class UserTypeSwitch extends StatefulWidget {
  const UserTypeSwitch({required this.store, Key? key}) : super(key: key);

  final EditAccountStore store;

  @override
  State<UserTypeSwitch> createState() => _UserTypeSwitchState();
}

class _UserTypeSwitchState extends State<UserTypeSwitch> {
  final List<bool> _selections = getType();

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return Observer(
          builder: (_) {
            return ToggleButtons(
              //constraints.maxwidth / numero de itens - a largura total da borda padrão
              borderWidth: 2,
              constraints: BoxConstraints.expand(
                  width: (constraints.maxWidth / 2) -3, height: 40),

              borderRadius: const BorderRadius.all(Radius.circular(16)),
              isSelected: _selections,
              children: const [
                Text('Particular'),
                Text('Profissional'),
              ],
              onPressed: widget.store.loading
                  ? null
                  : (int index) {
                      setState(() {
                        for (int buttonIndex = 0;
                            buttonIndex < _selections.length;
                            buttonIndex++) {
                          if (buttonIndex == index) {
                            _selections[buttonIndex] = true;
                            widget.store.setUserType(buttonIndex);
                          } else {
                            _selections[buttonIndex] = false;
                          }
                        }
                      });
                    },
            );
          },
        );
      },
    );
  }

  static List<bool> getType() {
    final UserManagerStore userManagerStore = GetIt.I<UserManagerStore>();
    if (userManagerStore.user?.type == UserType.particular) {
      return [true, false];
    } else {
      return [false, true];
    }
  }
}
