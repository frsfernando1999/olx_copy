import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get_it/get_it.dart';
import 'package:mobx/mobx.dart';
import 'package:xlo_mobx/pages/account/account_page.dart';
import 'package:xlo_mobx/pages/create/create_page.dart';
import 'package:xlo_mobx/pages/favorites/favorites_page.dart';
import 'package:xlo_mobx/pages/home/home_page.dart';
import 'package:xlo_mobx/pages/offline/offline_page.dart';
import 'package:xlo_mobx/stores/connectivity_store.dart';
import 'package:xlo_mobx/stores/home_store.dart';
import 'package:xlo_mobx/stores/page_store.dart';

class BasePage extends StatefulWidget {
  const BasePage({Key? key}) : super(key: key);

  @override
  State<BasePage> createState() => _BasePageState();
}

class _BasePageState extends State<BasePage> {
  final PageController pageController = PageController();

  List pageHistory = [];

  final PageStore pageStore = GetIt.I<PageStore>();
  final ConnectivityStore connectivityStore = GetIt.I<ConnectivityStore>();

  @override
  void initState() {
    super.initState();
    reaction<int>(
      (_) => pageStore.page,
      (page) => pageController.jumpToPage(page),
    );
    autorun((_){
    if(!connectivityStore.isConnected){
      Future.delayed(const Duration(milliseconds: 50)).then((value){
        showDialog(context: context, builder: (_) => const OfflineScreen());
      });
    }
    });
  }

  DateTime? currentBackPressTime;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WillPopScope(
        onWillPop: onWillPop,
        child: PageView(
          controller: pageController,
          physics: const NeverScrollableScrollPhysics(),
          children: [
            const HomePage(),
            const CreatePage(),
            Container(color: Colors.red),
            FavoritePage(),
            const AccountPage(),
          ],
        ),
      ),
    );
  }

  Future<bool> onWillPop() {
    if (pageStore.page == 0) {
      DateTime now = DateTime.now();
      if (currentBackPressTime == null ||
          now.difference(currentBackPressTime!) > const Duration(seconds: 2)) {
        currentBackPressTime = now;
        Fluttertoast.showToast(
            msg: 'Pressione voltar novamente para sair do aplicativo.',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.black38,
            fontSize: 16.0);
        return Future.value(false);
      }
      return Future.value(true);
    } else {
      if (pageStore.page != 0) {
        final HomeStore homeStore = GetIt.I<HomeStore>();
        homeStore.resetPage();
        pageStore.page = 0;
        return Future.value(false);
      } else {
        return Future.value(false);
      }
    }
  }
}
