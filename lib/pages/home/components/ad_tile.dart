import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:xlo_mobx/models/ad.dart';
import 'package:xlo_mobx/pages/ad/ad_page.dart';

import 'package:xlo_mobx/utils/extension.dart';
import 'package:xlo_mobx/utils/nav.dart';

class AdTile extends StatelessWidget {
  const AdTile(this.ad, {Key? key}) : super(key: key);

  final Ad? ad;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        push(context, AdPage(ad: ad));
      },
      child: SizedBox(
        height: 150,
        child: Card(
          clipBehavior: Clip.antiAlias,
          margin: const EdgeInsets.symmetric(horizontal: 8, vertical: 6),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
          elevation: 8,
          child: Row(
            children: [
              CachedNetworkImage(
                  height: max(0, 250),
                  imageUrl: ad?.images == []
                      ? 'https://triunfo.pe.gov.br/pm_tr430/wp-content/uploads/2018/03/sem-foto-300x300.jpg'
                      : ad?.images?.first,
                  fit: BoxFit.contain),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "${ad?.title}".capitalize(),
                      style: const TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                      ),
                    ),
                    Text(
                      "${ad?.price.formattedMoney()}",
                      style: const TextStyle(
                        fontSize: 19,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                    Text(
                      "${ad?.createdDate.formattedDate()} - ${ad?.address?.city?.name} - ${ad?.address?.uf?.initials}",
                      style: const TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w300,
                      ),
                    )
                  ],
                ),
              ))
            ],
          ),
        ),
      ),
    );
  }
}
