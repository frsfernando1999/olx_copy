import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';
import 'package:xlo_mobx/components/custom_drawer/custom_drawer.dart';
import 'package:xlo_mobx/pages/home/components/ad_tile.dart';
import 'package:xlo_mobx/pages/home/components/create_ad_button.dart';
import 'package:xlo_mobx/pages/home/components/search_dialog.dart';
import 'package:xlo_mobx/pages/home/components/top_bar.dart';
import 'package:xlo_mobx/stores/home_store.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final HomeStore homeStore = GetIt.I<HomeStore>();

  final ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: const CustomDrawer(),
        appBar: AppBar(
          title: Observer(builder: (_) {
            if (homeStore.search.isEmpty) {
              return const Text(
                'Todos os Anúncios',
                style: TextStyle(fontSize: 18),
              );
            } else {
              return GestureDetector(
                onTap: () => openSearch(context),
                child: LayoutBuilder(
                  builder: (_, BoxConstraints constraints) {
                    return SizedBox(
                      width: constraints.biggest.width,
                      child: Text(
                        "Pesquisando: ${homeStore.search}",
                        style: const TextStyle(fontSize: 18),
                      ),
                    );
                  },
                ),
              );
            }
          }),
          actions: [
            Observer(
              builder: (_) {
                if (homeStore.search.isEmpty) {
                  return IconButton(
                    onPressed: () {
                      openSearch(context);
                    },
                    icon: const Icon(
                      Icons.search,
                    ),
                  );
                } else {
                  return IconButton(
                    onPressed: () {
                      homeStore.setSearch('');
                    },
                    icon: const Icon(
                      Icons.close,
                    ),
                  );
                }
              },
            ),
          ],
        ),
        body: _body(context),
      ),
    );
  }

  Future<void> openSearch(BuildContext context) async {
    final search = await showDialog(
      context: context,
      builder: (_) => SearchDialog(
        currentSearch: homeStore.search,
      ),
    );
    if (search != null) {
      homeStore.setSearch(search);
    }
  }

  _body(BuildContext context) {
    return Column(
      children: [
        TopBar(),
        Expanded(
            child: Stack(
          children: [
            Observer(
              builder: (_) {
                if (homeStore.error != null) {
                  return errorMessage();
                } else if (homeStore.showProgress) {
                  return loadingHome();
                } else if (homeStore.adList.isEmpty) {
                  return noResultsHomeSearch();
                } else {
                  return homeContent();
                }
              },
            ),
            Positioned(
              bottom: -50,
              left: 0,
              right: 0,
              child: CreateAdButton(scrollController: scrollController),
            ),
          ],
        ))
      ],
    );
  }

  homeContent() {
    return ListView.builder(
      controller: scrollController,
        itemCount: homeStore.itemCount,
        itemBuilder: (_, index) {
          if (index < homeStore.adList.length) {
            return AdTile(homeStore.adList[index]);
          } else {
            homeStore.loadNextPage();
            return const SizedBox(
              height: 10,
              child: LinearProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.purple),
              ),
            );
          }
        });
  }

  noResultsHomeSearch() {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          Icon(
            Icons.announcement_outlined,
            color: Colors.white,
            size: 100,
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            'Hmm... Nenhum anúncio encontrado...',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.w700,
            ),
          ),
        ],
      ),
    );
  }

  errorMessage() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          Icon(
            Icons.error,
            color: Colors.white,
            size: 100,
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            'Ocorreu um erro!',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.w700,
            ),
          ),
        ],
      ),
    );
  }

  loadingHome() {
    return const Center(
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation(Colors.white),
      ),
    );
  }
}
