import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get_it/get_it.dart';
import 'package:mobx/mobx.dart';
import 'package:xlo_mobx/components/error_box.dart';
import 'package:xlo_mobx/pages/login/login_page.dart';
import 'package:xlo_mobx/pages/signup/components/field_title.dart';
import 'package:xlo_mobx/pages/signup/components/text_fields.dart';
import 'package:xlo_mobx/stores/signup_store.dart';
import 'package:xlo_mobx/stores/user_manager_store.dart';
import 'package:xlo_mobx/utils/mask.dart';
import 'package:xlo_mobx/utils/nav.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final SignupStore signupStore = SignupStore();
  final UserManagerStore userManagerStore = GetIt.I<UserManagerStore>();

  @override
  void initState() {
    super.initState();
    when((_) => userManagerStore.user != null, (){
      Fluttertoast.showToast(
          msg: "Muito prazer em conhecê-lo ${userManagerStore.user?.name}!",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 5,
          backgroundColor: Colors.black45,
          textColor: Colors.white,
          fontSize: 16.0
      );
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Cadastro'),
        centerTitle: true,
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
            elevation: 10,
            margin: const EdgeInsets.symmetric(horizontal: 32),
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Observer(builder: (_) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    ErrorBox(
                      message: signupStore.error,
                    ),
                    const FieldTitle(
                      title: 'Apelido',
                      subtitle: 'Como aparecerá em seus anúncios',
                    ),
                    SignUpTextFields(
                      enabled: !signupStore.loading,
                      hint: 'Exemplo: João S.',
                      function: signupStore.setName,
                      errorText: signupStore.nameError,
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    const FieldTitle(
                      title: 'E-mail',
                      subtitle: 'Enviaremos um e-mail de confirmação',
                    ),
                    SignUpTextFields(
                      enabled: !signupStore.loading,
                      hint: 'Exemplo: joao@gmail.com',
                      inputType: TextInputType.emailAddress,
                      function: signupStore.setEmail,
                      errorText: signupStore.emailError,
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    const FieldTitle(
                      title: 'Celular',
                      subtitle: 'Proteja sua conta',
                    ),
                    SignUpTextFields(
                      enabled: !signupStore.loading,
                      hint: 'Exemplo: (99) 99999-9999',
                      inputType: TextInputType.phone,
                      function: signupStore.setPhone,
                      errorText: signupStore.phoneError,
                      formatters: [
                        FilteringTextInputFormatter.digitsOnly,
                        MaskTextInputFormatter(mask: '(##) #####-####')
                      ],
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    const FieldTitle(
                      title: 'Senha',
                      subtitle: 'Use letras, números e caracteres especiais',
                    ),
                    SignUpTextFields(
                      enabled: !signupStore.loading,
                      hint: 'Digite uma senha',
                      correct: false,
                      obscure: true,
                      function: signupStore.setPass,
                      errorText: signupStore.passError,
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    const FieldTitle(
                      title: 'Confirmar Senha',
                      subtitle: 'Repita sua senha',
                    ),
                    SignUpTextFields(
                      enabled: !signupStore.loading,
                      hint: 'Repita o campo de senha',
                      correct: false,
                      obscure: true,
                      function: signupStore.setPass2,
                      errorText: signupStore.passError2,
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Container(
                      height: 40,
                      margin: const EdgeInsets.only(bottom: 4),
                      child: ElevatedButton(
                        onPressed: signupStore.signUpPressed,
                        child: signupStore.loading
                            ? const CircularProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation(Colors.white),
                              )
                            : const Text(
                                'Cadastrar',
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                      ),
                    ),
                    const Divider(
                      color: Colors.grey,
                      thickness: 0.3,
                    ),
                    Wrap(
                      alignment: WrapAlignment.spaceAround,
                      children: [
                        const Text(
                          'Já tem uma conta?',
                          style: TextStyle(fontSize: 16),
                        ),
                        GestureDetector(
                          onTap: () {
                            push(context, LoginScreen());
                          },
                          child: const Text(
                            'Faça login',
                            style: TextStyle(
                              decoration: TextDecoration.underline,
                              color: Colors.purple,
                              fontSize: 16,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                );
              }),
            ),
          ),
        ),
      ),
    );
  }
}
