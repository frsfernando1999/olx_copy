import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SignUpTextFields extends StatelessWidget {
  const SignUpTextFields({Key? key,
    required this.hint,
    this.inputType = TextInputType.text,
    this.obscure = false,
    this.correct = true,
    this.formatters, this.function, this.errorText,
    this.enabled = true,
  })
      : super(key: key);
  final String hint;
  final TextInputType inputType;
  final bool obscure;
  final bool correct;
  final List<TextInputFormatter>? formatters;
  final Function(String? value)? function;
  final String? errorText;
  final bool enabled;

  @override
  Widget build(BuildContext context) {
    return TextField(
      enabled: enabled,
      autocorrect: correct,
      obscureText: obscure,
      keyboardType: inputType,
      inputFormatters: formatters,
      decoration: InputDecoration(
        errorText: errorText,
        border: const OutlineInputBorder(),
        hintText: hint,
        isDense: true,
      ),
      onChanged: function,
    );
  }
}
