import 'package:xlo_mobx/models/address.dart';
import 'package:xlo_mobx/models/category.dart';
import 'package:xlo_mobx/models/user.dart';

enum AdStatus { pending, active, sold, deleted }

class Ad {
  Ad(
      {this.id,
      this.images,
      this.title,
      this.description,
      this.category,
      this.address,
      this.price,
      this.hidePhone,
      this.status = AdStatus.pending,
      this.createdDate,
      this.user,
      this.views});

  String? id;
  List? images = [];
  String? title;
  String? description;
  Category? category;
  Address? address;
  num? price;
  bool? hidePhone = false;
  AdStatus status = AdStatus.pending;
  DateTime? createdDate;
  User? user;
  int? views;

  @override
  String toString() {
    return 'Ad{id: $id, images: $images, title: $title, description: $description, category: $category, address: $address, price: $price, hidePhone: $hidePhone, status: $status, createdDate: $createdDate, user: $user, views: $views}';
  }
}
