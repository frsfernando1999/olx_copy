class Category {
  final String? id;
  final String? description;

  Category({
    this.id,
    this.description,
  });

  @override
  String toString() {
    return 'Category{id: $id, description: $description}';
  }
}
