import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';
import 'package:xlo_mobx/models/category.dart';
import 'package:xlo_mobx/repositories/parse_errors.dart';
import 'package:xlo_mobx/repositories/table_keys.dart';

class CategoryRepository {
  Future<Iterable<Category>?> getList() async {
    final queryBuilder = QueryBuilder(ParseObject(keyCategoryTable))
      ..orderByAscending(keyCategoryDescription);

    final response = await queryBuilder.query();

    if (response.success) {
      return response.results?.map((p) => mapParseToCategory(p));
    } else {
      throw ParseErrors.getDescription(response.error?.code as int);
    }
  }

  Category mapParseToCategory(ParseObject parseObject) {
    return Category(
      id: parseObject.objectId,
      description: parseObject.get(keyCategoryDescription),
    );
  }
}
