import 'dart:async';
import 'dart:io';

import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';
import 'package:xlo_mobx/models/ad.dart';
import 'package:path/path.dart' as path;
import 'package:xlo_mobx/models/address.dart';
import 'package:xlo_mobx/models/category.dart';
import 'package:xlo_mobx/models/user.dart';
import 'package:xlo_mobx/repositories/category_repository.dart';
import 'package:xlo_mobx/models/city.dart';
import 'package:xlo_mobx/models/uf.dart';
import 'package:xlo_mobx/repositories/parse_errors.dart';
import 'package:xlo_mobx/repositories/table_keys.dart';
import 'package:xlo_mobx/repositories/user_repository.dart';
import 'package:xlo_mobx/stores/filter_store.dart';
import 'package:xlo_mobx/stores/user_manager_store.dart';

class AdRepository {
  Future<List<Ad>?> getHomeAdList({
    FilterStore? filter,
    String? search,
    Category? category,
    int page = 0,
  }) async {
    try {
      final queryBuilder = QueryBuilder<ParseObject>(ParseObject(keyAdTable));

      queryBuilder.includeObject([keyAdOwner, keyAdCategory]);

      queryBuilder.setAmountToSkip(page * 10);
      queryBuilder.setLimit(10);

      queryBuilder.whereEqualTo(keyAdStatus, AdStatus.active.index);

      if (search != null && search.trim().isNotEmpty) {
        queryBuilder.whereContains(keyAdTitle, search, caseSensitive: false);
      }

      if (category != null && category.id != '*') {
        queryBuilder.whereEqualTo(
            keyAdCategory,
            (ParseObject(keyCategoryTable)..set(keyCategoryId, category.id))
                .toPointer());
      }

      switch (filter?.orderBy) {
        case OrderBy.price:
          queryBuilder.orderByAscending(keyAdPrice);
          break;
        case OrderBy.date:
        default:
          queryBuilder.orderByDescending(keyAdCreatedAt);
          break;
      }

      if (filter?.minPrice != null && (filter?.minPrice as int) > 0) {
        queryBuilder.whereGreaterThanOrEqualsTo(keyAdPrice, filter?.minPrice);
      }
      if (filter?.maxPrice != null && (filter?.maxPrice as int) > 0) {
        queryBuilder.whereLessThanOrEqualTo(keyAdPrice, filter?.maxPrice);
      }

      if (filter?.vendorType != null && (filter?.vendorType as int) > 0) {
        final userQuery = QueryBuilder<ParseUser>(ParseUser.forQuery());

        if (filter?.vendorType == vendorTypeParticular) {
          userQuery.whereEqualTo(keyUserType, UserType.particular.index);
        }

        if (filter?.vendorType == vendorTypeProfessional) {
          userQuery.whereEqualTo(keyUserType, UserType.profissional.index);
        }

        queryBuilder.whereMatchesQuery(keyAdOwner, userQuery);

        final response = await queryBuilder.query();
        if (response.success && response.results != null) {
          return response.results?.map((po) => mapParseToAd(po)).toList();
        } else if (response.success && response.results == null) {
          return [];
        } else {
          return Future.error(ParseErrors.getDescription(response.error!.code));
        }
      }
    } catch (e) {
      return Future.error('Falha de conexão');
    }
  }

  Future<void> save(Ad ad) async {
    try {
      final parseImages = await saveImages(ad.images);
      final parseAdUser = await ParseUser.currentUser();
      final adObject = ParseObject(keyAdTable);

      // Parse ACL gerencia as permissões de leitura e gravação de dados
      final parseAcl = ParseACL(owner: parseAdUser);

      if (ad.id != null) {
        adObject.set<String?>(keyAdId, ad.id);
      }
      parseAcl.setPublicReadAccess(allowed: true);
      parseAcl.setPublicWriteAccess(allowed: false);
      adObject.setACL(parseAcl);
      adObject.set<String?>(keyAdTitle, ad.title);
      adObject.set<String?>(keyAdDescription, ad.description);
      adObject.set<bool?>(keyAdHidePhone, ad.hidePhone);
      adObject.set<num?>(keyAdPrice, ad.price);
      // Caso queira aprovar instantaneamente quando editar,
      // basta adicionar um if (ad.id != null) antes do status
      adObject.set<int>(keyAdStatus, ad.status.index);
      adObject.set<String?>(keyAdDistrict, ad.address?.district);
      adObject.set<String?>(keyAdCity, ad.address?.city?.name);
      adObject.set(keyAdFederativeUnit, ad.address?.uf?.initials);
      adObject.set<String?>(keyAdPostalCode, ad.address?.cep);
      adObject.set<List<ParseFile>?>(keyAdImages, parseImages);

      adObject.set<ParseUser>(keyAdOwner, parseAdUser);

      adObject.set<ParseObject>(keyAdCategory,
          ParseObject(keyCategoryTable)..set(keyCategoryId, ad.category?.id));

      final response = await adObject.save();

      if (!response.success) {
        return Future.error(
            ParseErrors.getDescription(response.error?.code as int));
      }
    } catch (e) {
      return Future.error('Falha ao salvar anúncio.');
    }
  }

  Future<List<ParseFile>?>? saveImages(List? images) async {
    final parseImages = <ParseFile>[];
    if (images != null) {
      try {
        for (final image in images) {
          if (image is String) {
            final parseFile = ParseFile(File(path.basename(image)));
            parseFile.name = path.basename(image);
            parseFile.url = image;
            parseImages.add(parseFile);
          } else {
            final parseFile = ParseFile(image, name: path.basename(image.path));
            final response = await parseFile.save();
            if (!response.success) {
              return Future.error(
                  ParseErrors.getDescription(response.error?.code as int));
            }
            parseImages.add(parseFile);
          }
        }
        return parseImages;
      } on Exception {
        return Future.error('Falha ao salvar imagens');
      }
    }
    return null;
  }

  Ad mapParseToAd(ParseObject object) {
    return Ad(
      id: object.objectId,
      title: object.get<String?>(keyAdTitle),
      description: object.get<String?>(keyAdDescription),
      images: (object.get<List>(keyAdImages)?.map((e) => e['url']).toList()
          as List<dynamic>),
      hidePhone: object.get<bool?>(keyAdHidePhone),
      price: object.get<num?>(keyAdPrice),
      createdDate: object.createdAt,
      address: Address(
        district: object.get<String?>(keyAdDistrict),
        city: City(name: object.get<String?>(keyAdCity)),
        cep: object.get<String?>(keyAdPostalCode),
        uf: UF(initials: object.get<String?>(keyAdFederativeUnit)),
      ),
      views: object.get<int>(keyAdViews, defaultValue: 0),
      user: UserRepository().mapParseToUser(object.get<ParseUser>(keyAdOwner)),
      category:
          CategoryRepository().mapParseToCategory(object.get(keyAdCategory)),
      status: AdStatus.values[(object.get<int>(keyAdStatus) as int)],
    );
  }

  Future<List<Ad?>?>? getMyAds(UserManagerStore user) async {
    final currentUser = ParseUser('', '', '')..set(keyUserId, user.user?.id);
    final queryBuilder = QueryBuilder<ParseObject>(ParseObject(keyAdTable));

    queryBuilder.setLimit(100);
    queryBuilder.orderByDescending(keyAdCreatedAt);
    queryBuilder.whereEqualTo(keyAdOwner, currentUser.toPointer());
    queryBuilder.includeObject([keyAdCategory, keyAdOwner]);

    final response = await queryBuilder.query();

    if (response.success && response.results != null) {
      return response.results?.map((po) => mapParseToAd(po)).toList();
    } else if (response.success && response.results == null) {
      return [];
    } else {
      return Future.error(
          ParseErrors.getDescription(response.error?.code as int));
    }
  }

  Future<void> soldAd(Ad? ad) async {
    final parseObject = ParseObject(keyAdTable)..set(keyAdId, ad?.id);
    parseObject.set(keyAdStatus, AdStatus.sold.index);
    final response = await parseObject.save();
    if (!response.success) {
      return Future.error(
          ParseErrors.getDescription(response.error?.code as int));
    }
  }

  Future<void> deleteAd(Ad? ad) async {
    final parseObject = ParseObject(keyAdTable)..set(keyAdId, ad?.id);
    parseObject.set(keyAdStatus, AdStatus.deleted.index);
    final response = await parseObject.save();
    if (!response.success) {
      return Future.error(
          ParseErrors.getDescription(response.error?.code as int));
    }
  }
}
