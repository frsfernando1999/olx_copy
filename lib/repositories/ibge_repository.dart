import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:xlo_mobx/models/city.dart';
import 'package:xlo_mobx/models/uf.dart';

class IBGERepository {
  Future<List<UF>?> getUFList() async {
    final preferences = await SharedPreferences.getInstance();

    if (preferences.containsKey('UF_LIST')) {

      final j = json.decode(preferences.get('UF_LIST') as String);

      final ufList = j.map<UF>((j) => UF.fromJson(j)).toList();
      if (ufList != null) {
        ufList.sort(
            (UF a, UF b) => (a.name?.toLowerCase() as String).compareTo(b.name?.toLowerCase() as String));
        return ufList;
      }
    } else {

      const endpoint =
          'https://servicodados.ibge.gov.br/api/v1/localidades/estados/';

      try {
        final response = await Dio().get<List>(endpoint);

        preferences.setString('UF_LIST', json.encode(response.data));

        final ufList = response.data?.map<UF>((j) => UF.fromJson(j)).toList();
        if (ufList != null) {
          ufList.sort(
              (a, b) => (a.name?.toLowerCase() as String).compareTo(b.name?.toLowerCase() as String));
        }
        return ufList;
      } on DioError {
        return Future.error('Falha ao obter list de Estados');
      }
    }
    return null;
  }

  Future<List<City>?> getCityListFromApi(UF? uf) async {
    final String endpoint =
        'https://servicodados.ibge.gov.br/api/v1/localidades/estados/${uf?.id}/municipios';

    try {
      final response = await Dio().get<List>(endpoint);
      final cityList =
          response.data?.map<City>((j) => City.fromJson(j)).toList();

      if (cityList != null) {
        cityList.sort(
            (a, b) => (a.name?.toLowerCase() as String).compareTo(b.name?.toLowerCase() as String));
      }

      return cityList;
    } on DioError {
      return Future.error('Falha ao obter lista de cidades');
    }
  }
}
