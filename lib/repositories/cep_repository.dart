import 'dart:async';

import 'package:dio/dio.dart';
import 'package:xlo_mobx/models/address.dart';
import 'package:xlo_mobx/models/city.dart';
import 'package:xlo_mobx/repositories/ibge_repository.dart';

class CepRepository {
  Future<Address?> getAddressFromApi(String cep) async {
    if (cep.isEmpty) {
      return Future.error('CEP Inválido');
    }
    final cleanCep = cep.replaceAll(RegExp('[^0-9]'), '');
    if (cleanCep.length != 8) {
      return Future.error('CEP Inválido');
    }
    final endpoint = 'http://viacep.com.br/ws/$cleanCep/json';

    try {
      final response = await Dio().get<Map>(endpoint);

      if (response.data == null ||
          response.data!.containsKey('erro') && response.data!['error']) {
        return Future.error('CEP Inválido');
      }

      final ufList = await IBGERepository().getUFList();

      return Address(
          cep: response.data!['cep'],
          district: response.data!['bairro'],
          city: City(name: response.data!['localidade']),
          uf: ufList?.firstWhere((uf) => uf.initials == response.data!['uf']));
    } catch (e) {
      return Future.error('Falha ao buscar CEP');
    }
  }
}
