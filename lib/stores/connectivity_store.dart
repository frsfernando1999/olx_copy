import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:mobx/mobx.dart';

part 'connectivity_store.g.dart';

class ConnectivityStore = _ConnectivityStore with _$ConnectivityStore;

abstract class _ConnectivityStore with Store {
  _ConnectivityStore() {
    _setupListener();
  }

  @observable
  bool isConnected = false;

  @action
  _setupListener() {
    InternetConnectionChecker().checkInterval = const Duration(seconds: 5);
    InternetConnectionChecker().checkInterval.inSeconds;
    InternetConnectionChecker().onStatusChange.listen((status) {
      switch (status) {
        case InternetConnectionStatus.connected:
          isConnected = true;
          break;
        case InternetConnectionStatus.disconnected:
          isConnected = false;
          break;
      }
    });
  }
}
