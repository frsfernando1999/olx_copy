import 'package:get_it/get_it.dart';
import 'package:mobx/mobx.dart';
import 'package:xlo_mobx/models/ad.dart';
import 'package:xlo_mobx/models/address.dart';
import 'package:xlo_mobx/models/category.dart';
import 'package:xlo_mobx/repositories/ad_repository.dart';
import 'package:xlo_mobx/stores/cep_store.dart';
import 'package:xlo_mobx/stores/user_manager_store.dart';

part 'create_store.g.dart';

class CreateStore = _CreateStore with _$CreateStore;

abstract class _CreateStore with Store {
  _CreateStore([Ad? ad]) {
    if (ad != null) {
      id = ad.id;
      title = ad.title;
      description = ad.description;
      images = (ad.images as List).asObservable();
      category = ad.category;
      priceText = ad.price?.toStringAsFixed(2);

      if (ad.address != null) {
        cepStore = CepStore(ad.address?.cep);
      } else {
        cepStore = CepStore(null);
      }
    }
  }

  ObservableList images = ObservableList();

  @computed
  bool get imagesValid => images.isNotEmpty;

  String? get imagesError {
    if (!showErrors || imagesValid) {
      return null;
    } else {
      return 'Insira ao menos uma imagem';
    }
  }

  @observable
  String? id;

  @observable
  String? title = '';

  @action
  void setTitle(String value) => title = value;

  @computed
  bool get titleValid => (title as String).length >= 6;

  String? get titleError {
    if (!showErrors || titleValid) {
      return null;
    } else if (title == '') {
      return 'Campo Obrigatório';
    } else {
      return 'Título muito curto.';
    }
  }

  @observable
  String? description = '';

  @action
  void setDescription(String value) => description = value;

  @computed
  bool get descriptionValid =>
      (description as String).length >= 10 &&
      (description as String).length <= 250;

  String? get descriptionError {
    if (!showErrors || descriptionValid) {
      return null;
    } else if (description == '') {
      return 'Campo Obrigatório';
    } else {
      return 'Adicione mais detalhes!';
    }
  }

  @observable
  Category? category;

  @action
  void setCategory(Category? value) => category = value;

  @computed
  bool get categoryValid => category != null;

  String? get categoryError {
    if (!showErrors || categoryValid) {
      return null;
    } else {
      return 'Campo Obrigatório';
    }
  }

  CepStore cepStore = CepStore(null);

  @computed
  Address? get address => cepStore.address;

  bool get addressValid => address != null;

  String? get addressError {
    if (!showErrors || addressValid) {
      return null;
    } else {
      return 'Campo Obrigatório';
    }
  }

  @observable
  String? priceText = '';

  @action
  void setPrice(String value) => priceText = value;

  @computed
  num? get price {
    if ((priceText as String).contains(',')) {
      return num.tryParse((priceText as String).replaceAll(',', '.'));
    } else {
      return num.tryParse((priceText as String));
    }
  }

  bool get priceValid => price != null && (price as num) <= 9999999;

  String? get priceError {
    if (!showErrors || priceValid) {
      return null;
    } else if ((priceText as String).isEmpty) {
      return 'Campo Obrigatório';
    } else {
      return 'Preço Inválido';
    }
  }

  @observable
  bool hidePhone = false;

  @action
  void setHidePhone(bool? value) => hidePhone = (value as bool);

  @computed
  bool get formValid =>
      imagesValid &&
      titleValid &&
      descriptionValid &&
      categoryValid &&
      addressValid &&
      priceValid;

  @computed
  dynamic get sendPressed => formValid ? _send : null;

  @observable
  bool loading = false;

  @observable
  String? error;

  @observable
  bool savedAd = false;

  @action
  Future<void> _send() async {
    if(id != null){
      final ad = Ad();
      ad.id = id;
      ad.title = title;
      ad.description = description;
      ad.category = category;
      ad.price = price;
      ad.hidePhone = hidePhone;
      ad.images = images;
      ad.address = address;
      ad.user = GetIt
          .I<UserManagerStore>()
          .user;
      loading = true;
      try {
        await AdRepository().save(ad);
        savedAd = true;
      } catch (e) {
        error = e as String?;
      }
      loading = false;
    }else {
      final ad = Ad();
      ad.title = title;
      ad.description = description;
      ad.category = category;
      ad.price = price;
      ad.hidePhone = hidePhone;
      ad.images = images;
      ad.address = address;
      ad.user = GetIt
          .I<UserManagerStore>()
          .user;
    loading = true;
    try {
      await AdRepository().save(ad);
      savedAd = true;
    } catch (e) {
      error = e as String?;
    }
    loading = false;
    }
  }

  @observable
  bool showErrors = false;

  @action
  void invalidSendPressed() => showErrors = true;
}
