import 'package:get_it/get_it.dart';
import 'package:mobx/mobx.dart';
import 'package:xlo_mobx/repositories/user_repository.dart';
import 'package:xlo_mobx/stores/user_manager_store.dart';
import 'package:xlo_mobx/utils/extension.dart';

part 'login_store.g.dart';

class LoginStore = _LoginStore with _$LoginStore;

abstract class _LoginStore with Store {

  @observable
  String? email;

  @action
  void setEmail(String? value) => email = value;

  @computed
  bool get emailValid => email != null && email.isEmailValid();
  String? get emailError =>
      (email == null || emailValid) ? null : 'Email inválido';

  @observable
  String? pass;

  @action
  void setPass(String? value) => pass = value;

  @computed
  bool get passValid => pass != null && (pass as String).length >= 4;

  String? get passError =>
      (pass == null || passValid) ? null : 'Senha inválida';

  @computed
  dynamic get loginPressed => (emailValid && passValid && !loading) ? _login : null;

  @observable
  bool loading = false;

  @observable
  String? error;

  @action
  _login() async{
    loading = true;
    error = null;
    try {
      final user = await UserRepository().loginWithEmail(email, pass);
      GetIt.I<UserManagerStore>().setUser(user);
    } catch (e) {
      error = e as String?;
    }
    loading = false;

  }
}
