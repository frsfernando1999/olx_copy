import 'package:get_it/get_it.dart';
import 'package:mobx/mobx.dart';
import 'package:xlo_mobx/models/user.dart';
import 'package:xlo_mobx/repositories/user_repository.dart';
import 'package:xlo_mobx/stores/user_manager_store.dart';
import 'package:xlo_mobx/utils/extension.dart';

part 'signup_store.g.dart';

class SignupStore = _SignupStore with _$SignupStore;

abstract class _SignupStore with Store {
  @observable
  String? name;

  @action
  void setName(String? value) => name = value;

  @computed
  bool get nameValid => name != null && (name as String).trim().length >= 3;

  String? get nameError {
    if (name == null || nameValid) {
      return null;
    } else if (name == '' || (name as String).trim() != '') {
      return 'Campo Obrigatório';
    } else {
      return 'Insira ao menos 3 letras para o nome';
    }
  }

  @observable
  String? email;

  @action
  void setEmail(String? value) => email = value;

  @computed
  bool get emailValid => email != null && email.isEmailValid();

  String? get emailError {
    if (email == null || emailValid) {
      return null;
    } else if (email == '') {
      return 'Campo Obrigatório';
    } else {
      return 'E-mail inválido';
    }
  }

  @observable
  String? phone;

  @action
  void setPhone(String? value) => phone = value;

  @computed
  bool get phoneValid => phone != null && (phone as String).length >= 14;

  String? get phoneError {
    if (phone == null || phoneValid) {
      return null;
    } else if (phone == '') {
      return 'Campo Obrigatório';
    } else {
      return 'Celular inválido';
    }
  }

  @observable
  String? pass;

  @action
  void setPass(String? value) => pass = value;

  @computed
  bool get passValid => pass != null && (pass as String).length >= 6;

  String? get passError {
    if (pass == null || passValid) {
      return null;
    } else if (pass == '') {
      return 'Campo Obrigatório';
    } else {
      return 'Senha inválida';
    }
  }

  @observable
  String? pass2;

  @action
  void setPass2(String? value) => pass2 = value;

  @computed
  bool get passValid2 => pass2 != null && pass2 == pass;

  String? get passError2 {
    if (pass2 == null || passValid2) {
      return null;
    } else if (pass2 == '') {
      return 'Campo Obrigatório';
    } else if (pass2 != pass) {
      return 'Senha inválida';
    } else {
      return 'Senha inválida';
    }
  }

  @computed
  bool get isFormValid =>
      nameValid && emailValid && phoneValid && passValid && passValid2;

  @computed
  dynamic get signUpPressed => (isFormValid && !loading) ? signUp : null;

  @observable
  bool loading = false;

  @observable
  String? error;

  @action
  Future<void> signUp() async {
    loading = true;

    final user = User(
      name: name,
      email: email,
      phone: phone,
      password: pass,
    );

    try {
      final resultUser = await UserRepository().signUp(user);
      GetIt.I<UserManagerStore>().setUser(resultUser);
    } catch (e) {
      error = e as String?;
    }

    loading = false;
  }
}
