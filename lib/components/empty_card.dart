import 'package:flutter/material.dart';

class EmptyCard extends StatelessWidget {
  const EmptyCard({required this.text, Key? key}) : super(key: key);
  final String? text;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 16, horizontal: 32),
        child: Card(
          child: Column(
            children: [
              const Expanded(
                flex: 4,
                child: Icon(
                  Icons.border_clear,
                  size: 200,
                  color: Colors.purple,
                ),
              ),
              const Divider(color: Colors.grey,),
              Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      const Text('Hmmm...', style: TextStyle(color: Colors.orange, fontSize: 22, fontWeight: FontWeight.w600),),
                      Text('Você não possui nenhum item $text no momento.', textAlign: TextAlign.center,style: const TextStyle(fontSize: 16),),

                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
