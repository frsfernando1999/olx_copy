import 'package:flutter/material.dart';

class ErrorBox extends StatelessWidget {
  const ErrorBox({Key? key, this.message}) : super(key: key);

  final String? message;

  @override
  Widget build(BuildContext context) {
    if(message == null){
      return Container();
    }else {
      return Container(
        margin: const EdgeInsets.only(bottom: 8),
        padding: const EdgeInsets.all(12),
        decoration: BoxDecoration(
          color: Colors.red,
          borderRadius: BorderRadius.circular(8),
        ),
        child: Row(
          children: [
            const Icon(Icons.error_outline,color: Colors.white,size: 40,),
            const SizedBox(width: 16,),
            Expanded(child: Text(
              'Oops! $message. Por favor, tente novamente.',
              style: const TextStyle(
                color: Colors.white,
                fontSize: 14,
              ),
            ))
          ],
        ),
      );
    }
  }
}
