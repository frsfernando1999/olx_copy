String getPhoneRegion(String? phone) {
  final cleanPhone = phone?.replaceAll(RegExp('[^0-9]'), '');
  if (cleanPhone != null) {
    final int? ddd = int.tryParse(cleanPhone.substring(0, 2));
    switch (ddd) {
      case 61:
        return 'DDD$ddd - Distrito Federal';
      case 62:
      case 64:
        return 'DDD$ddd - Goiás';
      case 65:
      case 66:
        return 'DDD$ddd - Mato Grosso';
      case 67:
        return 'DDD$ddd - Mato Grosso do Sul';
      case 82:
        return 'DDD$ddd - Alagoas';
      case 71:
      case 73:
      case 74:
      case 75:
      case 77:
        return 'DDD$ddd - Bahia';
      case 85:
      case 88:
        return 'DDD$ddd - Ceara';
      case 98:
      case 99:
        return 'DDD$ddd - Maranhão';
      case 83:
        return 'DDD$ddd - Paraíba';
      case 81:
      case 87:
        return 'DDD$ddd - Pernambuco';
      case 86:
      case 89:
        return 'DDD$ddd - Piauí';
      case 84:
        return 'DDD$ddd - Rio Grande do Norte';
      case 79:
        return 'DDD$ddd - Sergipe';
      case 68:
        return 'DDD$ddd - Acre';
      case 96:
        return 'DDD$ddd - Amapá';
      case 92:
      case 97:
        return 'DDD$ddd - Amazonas';
      case 91:
      case 93:
      case 94:
        return 'DDD$ddd - Pará';
      case 69:
        return 'DDD$ddd - Rondônia';
      case 95:
        return 'DDD$ddd - Roraima';
      case 63:
        return 'DDD$ddd - Tocantins';
      case 27:
      case 28:
        return 'DDD$ddd - Espirito Santo';
      case 31:
      case 32:
      case 33:
      case 34:
      case 35:
      case 37:
      case 38:
        return 'DDD$ddd - Minas Gerais';
      case 21:
      case 22:
      case 24:
        return 'DDD$ddd - Rio de Janeiro';
      case 11:
      case 12:
      case 13:
      case 14:
      case 15:
      case 16:
      case 17:
      case 18:
      case 19:
        return 'DDD$ddd - São Paulo e região';
      case 41:
      case 42:
      case 43:
      case 44:
      case 45:
      case 46:
        return 'DDD$ddd - Paraná';
      case 51:
      case 53:
      case 54:
      case 55:
        return 'DDD$ddd - Rio Grande do Sul';
      case 47:
      case 48:
      case 49:
        return 'DDD$ddd - Santa Catarina';
      default:
        return 'Região Indeterminada';
    }
  }else{
    return 'Região Indeterminada';
  }
}
