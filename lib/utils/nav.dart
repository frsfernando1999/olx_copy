import 'package:flutter/material.dart';

Future push(BuildContext context, Widget page) {
  return Navigator.of(context)
      .push(MaterialPageRoute(builder: (_) => page,
      ));
}
